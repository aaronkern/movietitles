package com.aaronsapp

import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

case class JumbledMovieTitles(sortedJumbledTitle: String, originalJumbledTitle: String)
case class MovieTitles(sortedTitle: String, originalTitle: String)

class MovieTitleValidator {

  def sortJumbledTitles(jumbledDF: DataFrame, sparkSession: SparkSession): Dataset[JumbledMovieTitles] = {
    import sparkSession.implicits._

    //clean and sort jumbled titles
    val orderedJumbledArray = jumbledDF.select("title").map{m =>
      val readableTitle = m.getString(0)
      val charArray = readableTitle.replaceAll("[^A-Za-z0-9]", "").toCharArray
      JumbledMovieTitles(new String(charArray.sorted), readableTitle)
    }
    orderedJumbledArray
  }

  def sortValidTitles(validTitles: DataFrame, sparkSession: SparkSession): Dataset[MovieTitles] = {
    import sparkSession.implicits._

    val validTitlesSortedDS = validTitles.select("title").map { m =>
      val readableTitle = m.getString(0)
      val openParenIndex = readableTitle.indexOf('(')
      val endIndex = if (openParenIndex > 0) openParenIndex else readableTitle.length()
      val titleYearStripped = readableTitle.substring(0, endIndex)
      val charArray = titleYearStripped.replaceAll("[^A-Za-z0-9]", "").toCharArray
      val sortedTitle = new String(charArray.sorted)

      MovieTitles(sortedTitle, readableTitle)
    }

    validTitlesSortedDS
  }
}

object MovieTitleValidator {

  def main(args: Array[String]): Unit = {
    val conf: SparkConf = new SparkConf().setAppName("MovieTitleValidator").setMaster("local")

    val sparkSession: SparkSession =
      SparkSession
        .builder()
        .config(conf)
        .getOrCreate()

    val moviesDF: DataFrame = sparkSession.sqlContext
      .read.format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferschema", "true")
      .option("mode", "DROPMALFORMED")
      .load("src/main/resources/movies.csv")

    import sparkSession.implicits._

    val mtv = new MovieTitleValidator

    //clean and sort valid movie titles and remove year
    val validTitlesSortedDS = mtv.sortValidTitles(moviesDF, sparkSession)

    //convert passed jumbled movie titles into dataframe
    //val jumbledData = Array("y StTryoo 2", "amjuniJ", "uerpiGerm Ol Mnd", "eeFrB lliy").toSeq.toDF("title")
    val jumbledData = args.toSeq.toDF("title")

    //clean and sort jumbled movie titles
    val orderedJumbledListDS = mtv.sortJumbledTitles(jumbledData, sparkSession)

    //join jumbled and valid movie titles on cleaned and sorted titles
    val matchingTitles = validTitlesSortedDS
      .join(orderedJumbledListDS, orderedJumbledListDS("sortedJumbledTitle") === validTitlesSortedDS("sortedTitle"))
      .select(orderedJumbledListDS("originalJumbledTitle"), validTitlesSortedDS("originalTitle"))

    //right join if you want to still have access to all the tested jumbled titles
//    val allTitles = validTitlesSortedDS
//      .join(orderedJumbledListDS, orderedJumbledListDS("sortedJumbledTitle") === validTitlesSortedDS("sortedTitle"), "right")
//      .select(orderedJumbledListDS("originalJumbledTitle"), validTitlesSortedDS("originalTitle"))

    //ToDo: Save results out to a database to avoid pulling data back to driver
    matchingTitles.show()
    //allTitles.show()
  }
}

