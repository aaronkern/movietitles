import sbt.ExclusionRule

name := "SurveySettingSetter"

version := "0.1"

scalaVersion := "2.11.8"

val sparkVersion = "2.1.0"

parallelExecution in Global := false

concurrentRestrictions in Global += Tags.limit(Tags.Test, 1)

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "com.typesafe.play" %%  "play-json"     % "2.4.6",
  "com.zaxxer" % "HikariCP" % "2.6.1",
  "org.scalaj" %% "scalaj-http" % "2.3.0",
  "org.elasticsearch" % "elasticsearch-hadoop" % "6.3.0",
  "com.typesafe" % "config" % "1.3.1",
  "org.scalatest" %% "scalatest" % "3.0.1"
)

resolvers ++= Seq(
  "confluent" at "http://packages.confluent.io/maven/",
  "softprops-maven" at "http://dl.bintray.com/content/softprops/maven"
)